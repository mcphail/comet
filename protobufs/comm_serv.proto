syntax = "proto2";

package protobufs.comm_serv;

enum MessageSort {
    MESSAGE_SORT = 1;
}

enum MessageType {
    UNKNOWN_MESSAGE = 0;
    LIBRARY_INFO_REQUEST = 1;
    LIBRARY_INFO_RESPONSE = 2;
    AUTH_INFO_REQUEST = 3;
    AUTH_INFO_RESPONSE = 4;
    GET_USER_STATS_REQUEST = 15;
    GET_USER_STATS_RESPONSE = 16;
    GET_USER_ACHIEVEMENT_REQUEST = 23;
    GET_USER_ACHIEVEMENT_RESPONSE = 24;
    UNLOCK_USER_ACHIEVEMENT_REQUEST = 25;
    UNLOCK_USER_ACHIEVEMENT_RESPONSE = 26;
    CLEAR_USER_ACHIEVEMENT_REQUEST = 27;
    CLEAR_USER_ACHIEVEMENT_RESPONSE = 28;
}

enum ValueType { // unverified
    VALUE_TYPE_UNDEFINED = 0;
    VALUE_TYPE_INT = 1; // verified
    VALUE_TYPE_FLOAT = 2;
    VALUE_TYPE_NONE = 3;
}

message LibraryInfoRequest {
    optional uint32 bitness = 1; // 0 - 2
    optional uint32 compiler_type = 2; // 0 - 3
    optional string compiler_version = 3;
}

message LibraryInfoResponse {
    optional string location = 1;
    optional uint32 update_status = 2; // 0 - 3
}

message AuthInfoRequest {
    required string client_id = 1;
    required string client_secret = 2;
    optional uint32 platform_type = 3; // 0 - 2
    optional string steam_ticket = 4;
    optional uint32 game_pid = 5;
}

message AuthInfoResponse {
    required string refresh_token = 1;
    required uint32 environment_type = 2;
    required uint64 user_id = 3;
    required string user_name = 4;
    required uint32 region = 5; // 0 - 1
}

message GetUserStatsRequest {
    required fixed64 user_id = 1;
}

message GetUserStatsResponse {
    message UserStat {
        required fixed64 stat_id = 1;
        optional string key = 2;
        optional uint32 value_type = 3; // 0 - 3
        optional float float_value = 4;
        optional uint32 int_value = 5;
        optional uint32 window_size = 6;
        optional float float_default_value = 7;
        optional uint32 int_default_value = 8;
        optional float float_min_value = 9;
        optional uint32 int_min_value = 10;
        optional float float_max_value = 11;
        optional uint32 int_max_value = 12;
        optional float float_max_change = 13;
        optional uint32 int_max_change = 14;
        optional bool increment_only = 15;
    }

    repeated UserStat user_stats = 1;
}

message GetUserAchievementsRequest {
    required fixed64 user_id = 1;
}

message GetUserAchievementsResponse {
    message UserAchievement {
        required fixed64 achievement_id = 1;
        required string key = 2;
        optional string name = 3;
        optional string description = 4;
        optional string image_url_locked = 5;
        optional string image_url_unlocked = 6;
        optional bool visible_while_locked = 7;
        optional fixed32 unlock_time = 8;
        optional float rarity = 9; // confirmed
        optional string rarity_level_description = 10;
        optional string rarity_level_slug = 11;
    }

    repeated UserAchievement user_achievements = 1;
    optional string language = 2;
    optional string achievements_mode = 3;
}

message UnlockUserAchievementRequest {
    required fixed64 achievement_id = 1;
    required fixed32 time = 2;
}

message ClearUserAchievementRequest {
    required fixed64 achievement_id = 1;
}

