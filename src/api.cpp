#include "stx/request.hpp"
#include "stx/string.hpp"

#include "api.hpp"
#include "token.hpp"
#include "config.hpp"



static void checkError(stx::HttpResponse& resp)
{
    if (!resp.ok()) {
        try {
            stx::json error_doc = resp.getJson();
            throw ApiError(
                error_doc["error"],
                error_doc["error_description"]
            );
        } catch (stx::json::exception) {
            resp.throwStatus();
        }
    }
}


GogApi::GogApi(Token& token) :
    m_token(token)
{

}

stx::HttpRequest GogApi::defaultRequest(std::string url)
{
    stx::HttpRequest req;
    req.setUrl(url);
    req.setHeader("Authorization", "Bearer " + m_token.access_token);
    req.setHeader("User-Agent", USER_AGENT);
    return req;
}

GogUser GogApi::getUser()
{
    stx::HttpRequest req = defaultRequest("https://embed.gog.com/userData.json");
    stx::json user_doc = req.request().getJson();

    GogUser user;
    user.username = user_doc["username"];
    user.web_user_id = user_doc["userId"];
    std::string galaxy_id_str = user_doc["galaxyUserId"];
    user.galaxy_user_id = GalaxyID(GalaxyID::USER, std::stoll(galaxy_id_str));
    user.email = user_doc["email"];
    user.avatar = user_doc["avatar"];

    return user;
}

static ValueType stringToValueType(std::string type_str)
{
    if (type_str == "int") {
        return VALUETYPE_INT;
    }
    throw std::runtime_error("Unknown value type");
}

std::vector<GogUserStat> GogApi::getUserStats(GalaxyID user_id)
{
    std::string stats_url = stx::format(
        "https://gameplay.gog.com/clients/%/users/%/stats",
        m_token.client_id, user_id.baseID());
    stx::HttpRequest req = defaultRequest(stats_url);
    stx::HttpResponse resp = req.request();
    checkError(resp);

    stx::json resp_doc = resp.getJson();
    stx::json stats_array = resp_doc["items"];
    std::vector<GogUserStat> stats_list;

    for (stx::json stat_o : stats_array) {
        GogUserStat stat;

        stat.stat_id = std::stoll(stat_o["stat_id"].get<std::string>());
        stat.stat_key = stat_o["stat_key"];
        stat.stat_type = stringToValueType(stat_o["type"]);
        if (!stat_o["window"].is_null()) {
            stat.window_size = stat_o["window"];
        } else {
            stat.window_size = 0;
        }
        stat.increment_only = stat_o["increment_only"]; // default false

        if (stat.stat_type == VALUETYPE_INT) {
            stat.value.i = stat_o["value"]; // def 0
            if (!stat_o["default_value"].is_null()) {
                stat.default_value.i = stat_o["default_value"];
            } else {
                stat.default_value.i = 0;
            }
            if (!stat_o["min_value"].is_null()) {
                stat.min_value.i = stat_o["min_value"]; // 0
            } else {
                stat.min_value.i = 0;
            }
            if (!stat_o["max_value"].is_null()) {
                stat.max_value.i = stat_o["max_value"]; // def 1000000
            } else {
                stat.max_value.i = 1000000;
            }
            if (!stat_o["max_change"].is_null()) {
                stat.max_change.i = stat_o["max_change"];
            } else {
                stat.max_change.i = 1;
            }
        }

        stats_list.push_back(stat);
    }

    return stats_list;
}

GogUserAchievementList GogApi::getUserAchievements(GalaxyID user_id)
{
    std::string achieve_url = stx::format(
        "https://gameplay.gog.com/clients/%/users/%/achievements",
        m_token.client_id, user_id.baseID());
    stx::HttpRequest req = defaultRequest(achieve_url);
    stx::HttpResponse resp = req.request();
    checkError(resp);

    stx::json resp_doc = resp.getJson();
    stx::json achieve_array = resp_doc["items"];
    GogUserAchievementList achieve_list;

    for (stx::json achieve_o : achieve_array) {
        GogUserAchievement achieve;

        achieve.achievement_id = std::stoll(achieve_o["achievement_id"].get<std::string>());
        achieve.achievement_key = achieve_o["achievement_key"];
        achieve.name = achieve_o["name"];
        achieve.description = achieve_o["description"];
        achieve.image_url_locked = achieve_o["image_url_locked"];
        achieve.image_url_unlocked = achieve_o["image_url_unlocked"];
        achieve.visible_while_locked = achieve_o["visible"];
        if (achieve_o["date_unlocked"].is_string()) {
            std::string date_str = achieve_o["date_unlocked"];
            std::tm timestruct = stx::parseDate(date_str, "%Y-%m-%dT%H:%M:%S");
            achieve.unlock_time = std::mktime(&timestruct);
        } else {
            achieve.unlock_time = 0;
        }
        achieve.rarity = achieve_o["rarity"];
        achieve.rarity_desc = achieve_o["rarity_level_description"];
        achieve.rarity_slug = achieve_o["rarity_level_slug"];

        achieve_list.items.push_back(achieve);
    }
    achieve_list.mode = resp_doc["achievements_mode"];
    achieve_list.language = "en-US";

    return achieve_list;
}

void GogApi::setUserAchievement(GalaxyID user_id, uint64_t achievement_id, std::time_t unlock_time)
{
    stx::json doc;
    if (unlock_time != 0) {
        std::tm timestruct = *std::gmtime(&unlock_time);
        std::string timestr = stx::formatDate(timestruct, "%Y-%m-%dT%H:%M:%S+0000");
        doc["date_unlocked"] = timestr;
    } else {
        doc["date_unlocked"] = stx::json(); // null
    }
    std::string post_str = doc.dump();

    std::string achieve_url = stx::format(
        "https://gameplay.gog.com/clients/%/users/%/achievements/%",
        m_token.client_id, user_id.baseID(), achievement_id);
    stx::HttpRequest req = defaultRequest(achieve_url);
    req.setHeader("Content-Type", "application/json");
    req.setPayload(stx::bytes(post_str));
    stx::HttpResponse resp = req.request();
    checkError(resp);
}

ApiError::ApiError(std::string error_name_, std::string error_desc_) :
    std::runtime_error(
        stx::format("API error (%): %", error_name_, error_desc_)
    ),
    error_name(error_name_),
    error_desc(error_desc_)
{

}
