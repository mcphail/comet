#pragma once

const char* const USER_AGENT = "GOGGalaxyCommunicationService/0.1.0.0 (Linux_64bit)";
// TODO: follow XDG
const char* const TOKEN_PATH = "~/.local/share/comet/token.json";
const char* const PEER_PATH = "~/.local/share/comet/peer";
