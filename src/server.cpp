#include <string>

#include "stx/socketserver.hpp"

#include "server.hpp"
#include "token.hpp"



GogServer::GogServer(stx::NetworkAddress addr, stx::ConnectionHandler handler, Token token) :
    stx::TcpServer(addr, handler),
    m_token(token)
{

}

Token GogServer::tokenFor(std::string client_id, std::string client_secret)
{
    Token app_token(client_id, client_secret);
    app_token.refreshFrom(m_token.refresh_token);

    return app_token;
}
