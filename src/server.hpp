#pragma once
#include <string>
#include <map>
#include <mutex>
#include <memory>

#include "stx/socketserver.hpp"

#include "token.hpp"



class GogServer : public stx::TcpServer
{
public:
    GogServer(stx::NetworkAddress addr, stx::ConnectionHandler handler, Token token);

    Token tokenFor(std::string client_id, std::string client_secret);

private:
    Token m_token;
};
