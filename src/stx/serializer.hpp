#pragma once
#include <sstream>
#include <cassert>
#include <stdint.h>
#include <type_traits>

#include "traits.hpp"
#include "bytearray.hpp"
#include "stream.hpp"



namespace stx {

class TextSerializer;

TextSerializer& operator<<(TextSerializer& ser, const std::string& val);
TextSerializer& operator<<(TextSerializer& ser, const char* val);
TextSerializer& operator<<(TextSerializer& ser, int64_t val);
TextSerializer& operator<<(TextSerializer& ser, int32_t val);
TextSerializer& operator<<(TextSerializer& ser, int16_t val);
TextSerializer& operator<<(TextSerializer& ser, uint64_t val);
TextSerializer& operator<<(TextSerializer& ser, uint32_t val);
TextSerializer& operator<<(TextSerializer& ser, uint16_t val);
TextSerializer& operator<<(TextSerializer& ser, float val);
TextSerializer& operator<<(TextSerializer& ser, double val);
TextSerializer& operator<<(TextSerializer& ser, long double val);
TextSerializer& operator<<(TextSerializer& ser, unsigned char val);
TextSerializer& operator<<(TextSerializer& ser, char val);
TextSerializer& operator<<(TextSerializer& ser, bool val);
TextSerializer& operator<<(TextSerializer& ser, const ByteArray& val);

template<typename T>
TextSerializer& operator<<(TextSerializer& ser, const T& arg);
template<typename T, typename std::enable_if<stx::is_iterable<T>::value>::type = 0>
TextSerializer& operator<<(TextSerializer& ser, const T& cont);



class TextSerializer
{
public:
    constexpr TextSerializer() :
        stream(nullptr)
    {

    }

    constexpr TextSerializer(BaseStream& stream_) :
        stream(&stream_)
    {

    }

    void writeString(const std::string& str);
    std::string readString(int64_t size);

    BaseStream* getStream();

    template<typename T, typename... Targs>
    void print(T first, Targs... args)
    {
        *this << first;
        print_space(args...);
    }

    void printf(const char* format)
    {
        *this << format;
    }

    template<typename T, typename... Targs>
    void printf(const char* format, T value, Targs... args)
    {
        for (; *format != '\0'; format++) {
            if (*format == '%') {
               *this << value;
               printf(format + 1, args...);
               return;
            }
            *this << *format;
        }
    }

private:
    template<typename T>
    void writeToString(T val)
    {
        assert(stream);
        writeString(std::to_string(val));
    }

    template<typename T>
    void print_space(T last)
    {
        *this << ' ' << last;
    }

    template<typename T, typename... Targs>
    void print_space(T middle, Targs... args)
    {
        *this << ' ' << middle;
        print_space(args...);
    }

protected:
    BaseStream* stream;
};


// Fallback serializer
template<typename T>
TextSerializer& operator<<(TextSerializer& ser, const T& arg)
{
    std::stringstream ss;
    ss << arg;
    std::string str = ss.str();
    ser.writeString(ss.str());
    return ser;
}

// Iterable serializer
template<typename T, typename std::enable_if<stx::is_iterable<T>::value>::type = 0>
TextSerializer& operator<<(TextSerializer& ser, const T& cont)
{
    ser << '[';
    auto begin = cont.cbegin();
    auto end = cont.cend();
    for (auto it = begin; it != end; ++it) {
        if (it != begin) {
            ser << ", ";
        }
        ser << *it;
    }
    ser << ']';

    return ser;
}

static TextSerializer txtout(stx::sout);
static TextSerializer txtin(stx::sin);
static TextSerializer txterr(stx::serr);

}
