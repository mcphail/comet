#pragma once
#include <string>
#include <sstream>
#include <ostream>
#include <ctime>

#include "bytearray.hpp"



namespace stx {

inline void tprintf(std::ostream& stream, const char* format)
{
    stream << format;
}

template<typename T, typename... Targs>
void tprintf(std::ostream& stream, const char* format, T value, Targs... args)
{
    for (; *format != '\0'; format++) {
        if (*format == '%') {
           stream << value;
           tprintf(stream, format+1, args...);
           return;
        }
        stream << *format;
    }
}

template<typename... Targs>
std::string format(std::string fmt, Targs... args)
{
    std::stringstream ss;
    tprintf(ss, fmt.c_str(), args...);
    return ss.str();
}

inline std::string str(const ByteArray& ba)
{
    return std::string(ba.data(), ba.size());
}

inline ByteArray bytes(const std::string str)
{
    const char* start = str.data();
    return ByteArray(start, start + str.size());
}

bool startswith(char c, const std::string& str);
bool startswith(const std::string& start, const std::string& str);
bool endswith(char c, const std::string& str);
bool endswith(const std::string& end, const std::string& str);
std::string hex(const ByteArray& ba);
std::tm parseDate(std::string input, const std::string& fmt);
std::string formatDate(std::tm date, const std::string& fmt);

}
